let buku = [];

window.addEventListener('load', function() {
    buku = JSON.parse(localStorage.getItem('data_buku')) || [], viewBuku(buku);
    $('form').onsubmit = storeBuku;
    $('.toolbar>.form-input').onchange = cariBuku;
});

function viewBuku(buku) {
    const sudahDibaca = $('.sudah-dibaca'),belumDibaca = $('.belum-dibaca');

    sudahDibaca.innerHTML = '', belumDibaca.innerHTML = '';

    for (let data_buku of buku) {
        let item = $new('div', { id: data_buku.id, css: ['buku'] });
        let title = $new('h4', { text: `${data_buku.title}` });
        let author = $new('p', { text: `Penulis : ${data_buku.author}` });
        let year = $new('p', { text: `Tahun : ${data_buku.year}` });
        let group = $new('button', { css: ['button', 'button-success'], text: `${data_buku.isComplete ? 'Belum selesai dibaca' : 'Selesai dibaca'}`, action: updateBaca });
        let remove = $new('button', { css: ['button', 'button-danger'], text: 'Hapus Buku', action: hapusBuku });

        [title, author, year, group, remove].forEach(e => {
            item.appendChild(e);
        });

        data_buku.isComplete ? sudahDibaca.appendChild(item) : belumDibaca.appendChild(item);
    }

    !sudahDibaca.hasChildNodes() ? sudahDibaca.innerHTML = 'Buku masih kosong' : 0;
    !belumDibaca.hasChildNodes() ? belumDibaca.innerHTML = 'Buku masih kosong' : 0;
}

// update
function updateBaca(e) {
    const position = buku.findIndex(i => i.id == e.target.parentNode.id);
    buku[position].isComplete = !buku[position].isComplete;
    simpanBuku();
    viewBuku(buku);
}
// end update

// delete
var closeDel = document.getElementById('closeDel');
var modalDelete = document.getElementById('modalDelete');
closeDel.addEventListener('click', closeModalDel);
document.getElementById('batalDel').addEventListener('click', closeModalDel);
function closeModalDel() {
    modalDelete.style.display = 'none';
}
function hapusBuku(e) {
    modalDelete.style.display = 'block';
    document.getElementById('delete').addEventListener('click', function(){
        modalDelete.style.display = 'none';
        const position = buku.findIndex(i => i.id == e.target.parentNode.id);
        buku.splice(position, 1);
        simpanBuku();
        viewBuku(buku);
    })
}
// end delete

// insert
var modal = document.getElementById('modalTambah');
var modalBtn = document.getElementById('modalBtn');
var closeBtn = document.getElementsByClassName('closeBtn')[0];
modalBtn.addEventListener('click', openModal);
closeBtn.addEventListener('click', closeModal);
window.addEventListener('click', outsideClick);

function openModal() {
    modal.style.display = 'block';
}

function closeModal() {
    modal.style.display = 'none';
    $('#title').value = "";
    $('#author').value = "";
    $('#year').value = "";
    $('#isComplete').checked = "";
}

function outsideClick(e) {
    if (e.target == modal) {
        modal.style.display = 'none';
    }
    if (e.target == modalDelete) {
        modalDelete.style.display = 'none';
    }
}
function storeBuku() {
    const position = $('form').id;
    let model = {
        id: +new Date(),
        title: $('#title').value,
        author: $('#author').value,
        year: $('#year').value,
        isComplete: $('#isComplete').checked
    }

    if (position) {
        buku[position].title = model.title;
        buku[position].author = model.author;
        buku[position].year = model.year;
        buku[position].isComplete = model.isComplete;
    } else {
        buku.push(model);
    }
    simpanBuku();
    viewBuku(buku);
}
// end insert

// search
function cariBuku(e) {
    e = e.target.value;
    viewBuku(buku.filter(data_buku => {
        return data_buku.title.toLowerCase().includes(e.toLowerCase());
    }));
}
// end search

// simpan storage
function simpanBuku() {
    localStorage.setItem('data_buku', JSON.stringify(buku));
}

function $new(e, a) {
    e = document.createElement(e);
    a.id ? e.id = a.id : 0;
    a.text ? e.innerText = a.text : 0;
    a.action ? e.addEventListener('click', a.action) : 0;
    a.css ? a.css.forEach(css => e.classList.add(css)) : 0;
    return e;
}

function $(e) {
    e = document.querySelectorAll(e);
    return e.length >= 1 ? e[0] : e;
}